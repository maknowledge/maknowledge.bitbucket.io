========
misc
========

環境変数の取得の仕方
========================

_ENVを直に参照
-------------------
::

   $hoge = $_ENV['some_attribute_name'];

env ヘルパを使用
--------------------

環境変数名(``some_attribute_name`` )が存在しない場合は ``default_value`` を返す
::

   $hoge = env("some_attribute_name", "default_value")


設定ファイルの作成
========================

* 以下のディレクトリ構造で設定ファイルを作成する。

::

   project_root
      └── config
         └── hoge.php

* hoge.php の設定の書き方

::

   <?php

   return [
       'a' => 1,

       'happy' => env('SOME_VALUE', 100),
    
       'new' => [
           'year' => 'good',
       ],
   ];

* 設定の呼び出し方

::

   config('hoge.a') # -> 1
   config('hoge.happy') # -> SOME_VALUE が未定義なら 100
   config('hoge.new.year') -> # 'good'


resource, resource collection
================================

* コントローラ側で Resource , ResourceCollection にメタデータを追加する方法


additional を使う

::

   return (new UserCollection(User::all()->load('roles')))
                ->additional(['meta' => [
                    'key' => 'value',
                ]]);

where メソッドの場所
========================

* Eloquant の Model クラスから呼び出す where等のメソッドはどこにあるのか

``Illuminate\Database\Eloquent\Model;`` のメソッド一覧には存在しない。

これらのメソッドは、``\Illuminate\Database\Eloquent\Builder`` に存在する。

これらのメソッドは、Model クラス内の以下のマジックメソッド（``__call`` と ``__callstatic`` ）を経由して実行されている。

Model クラスに存在しない where メソッド を呼び出すと、クラスが instantinate されている場合は ``__call`` が呼ばれる。（static に呼び出された場合は、 ``__callstatic`` が呼び出され、その中で Model クラス が instantinate されてから ``__call`` が呼ばれる。）

その中で、newQuery メソッドを呼び出し ``\Illuminate\Database\Eloquent\Builder`` のインスタンスを返し、目的のメソッドが呼ばれる。

また、``\Illuminate\Database\Eloquent\Builder`` にもメソッドがない場合は、さらに ``\Illuminate\Database\Eloquent\Builder`` 内 の マジックメソッド（``__call``）によって、 ``\Illuminate\Database\Query\Builder`` 内のメソッドが呼ばれる。

上記の理由から ``Illuminate\Database\Eloquent\Model;`` → ``\Illuminate\Database\Eloquent\Builder``　→ ``\Illuminate\Database\Query\Builder`` の順にメソッドが存在すれば実行される。


::


    /**
     * Handle dynamic method calls into the model.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        if (in_array($method, ['increment', 'decrement'])) {
            return $this->$method(...$parameters);
        }

        return $this->newQuery()->$method(...$parameters);
    }

    /**
     * Handle dynamic static method calls into the method.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */
    public static function __callStatic($method, $parameters)
    {
        return (new static)->$method(...$parameters);
    }



