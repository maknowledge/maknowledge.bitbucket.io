=====================
Install（作成中）
=====================


::

    # SELinuxの無効化
    $ vi /etc/sysconfig/selinux
     SELINUX=disabled を追記

    # vagrant reload で再起動
    # SELinuxが無効になっていることを確認(Disabled)
    $ getenforce

    # epel-release を追加
    $ yum install -y epel-release

    # remi リポジトリの追加
    $ rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-7.rpm

    $ yum install -y --enablerepo=remi,remi-php72 php php-devel php-mbstring php-pdo php-gd php-xml php-pdo_pgsql php-pecl-xdebug zip unzip

    # firewalld 関連
    $ sudo systemctl start firewalld
    $ sudo systemctl enable firewalld
    $ sudo firewall-cmd --add-service=http --permanent
    $ sudo firewall-cmd --add-service=https --permanent
    $ sudo firewall-cmd --add-service=ssh --permanent
    $ sudo systemctl restart firewalld

    # httpd関連
    $ sudo systemctl start httpd
    $ sudo systemctl enable httpd



    # composerインストール
    $ curl -sS https://getcomposer.org/installer | php
    $ mv composer.phar /usr/local/bin/composer



    # laravel のインストールディレクトリ作成
    mkdir /var/www/php
    chmod 777 /var/www/php

    # とりあえず一度再起動( vagrant reload )

    $ laravel インストール
    ＄composer create-project laravel/laravel:5.5 /var/www/php



    # Appacheの設定とか
    $ ln -s /var/www/php/public /var/www/html/foo

    $ sudo chmod 777 /var/www/php/storage -R

    <Directory "/var/www/html/foo">
    AllowOverride All
    Require all granted
    </Directory>

    <Directory "/var/www/html/*">
    AllowOverride All
    Require all granted
    </Directory>
