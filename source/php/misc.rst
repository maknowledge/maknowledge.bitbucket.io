=======
misc
=======

Installation(for Mac with Homebrew)
=======================================

::

   $ brew install php
   $ brew install homebrew/php/php72-xdebug

Configure
=============

* ``/usr/local/etc/php/7.2/conf.d/ext-xdebug.ini`` に以下を追記。


::

    zend_extension="/usr/lib64/php/modules/xdebug.so"
    xdebug.remote_enable=1
    xdebug.remote_autostart=1
    ;xdebug.remote_connect_back = 1
    xdebug.profiler_enable=1
    xdebug.profiler_output_name=cachegrind.out.%t
    xdebug.profiler_output_dir=/tmp
    xdebug.max_nesting_level=10000
    xdebug.remote_log="/tmp/xdebug.log"
    xdebug.remote_host=192.168.100.103
    xdebug.remote_port=9000


xcodeでのデバッグ（リモート）
=================================

* ``launch.json`` に以下を追記

::

    "pathMappings": {
        "{ゲストOS上のパス}" : "${workspaceRoot}"
    }