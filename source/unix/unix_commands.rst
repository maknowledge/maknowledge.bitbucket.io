=================
Unix Commands
=================

検索
======

::

   # find {対象ディレクトリ} -name {対象ファイル名}
   $ find / -name foo.txt

コマンドの場所
===============

::

   # which {コマンド名}
   $ which vi


diskの状態
=============

::

    $ df -h

定期実行(crontab)
====================

Options
-----------
::

   # 状態を表示
   $ crontab -l

   # 全削除
   $ crontab -r

   # エディタで編集
   $ crontab -e

   # ユーザ指定
   $ crontab -e -r root


書式(例)
-----------
::

    0 * * * * echo "test" # 毎時 0 分
    0 0 * * * echo "test" # 毎日 0:00
    0 0 1 * * echo "test" # 毎月 1 日 0:00
    0 0 1 1 * echo "test" # 毎年 1/1 0:00
    0 0 * * 1 echo "test" # 毎週 月曜日
    */1 * * * * echo "test" # 毎分
    0,30 * * * * echo "test" # 毎時 0 分 と 30分
    0 0 * * 1-5 echo "test" # 毎週 月曜日から金曜日



メールを飛ばさないようにする
-------------------------------

* MAILTO=""　を追記

所有者変更
============

ファイル
----------

::

    $ chown {USER}:{GROUP} {file_path}

ディレクトリ（再帰）
----------------------

::

    $ chown -R {USER}:{GROUP} {directory_path}


改行コードをフィルタ
========================

::

   $ man tar | col -bx > tar_help
   

Options
------------

* `-b` : バックスペースを取り除く
* `-x` : タブの代わりに複数の半角文字列に置き換える
* `-p` : そのまま出力


ssh-keygen
==================

秘密鍵から公開鍵を生成
-----------------------------

::

   $ ssh-keygen -yf {path_to_secret_key} > public_key


findで見つけて削除
======================

::

   $ find . -name ".txt" | xargs rm


Linux Run Command As Another User
========================================

::

    $ sudo runuser -l postgres -c 'psql -U postgres'