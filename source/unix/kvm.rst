======
KVM
======


Installation
=================

::

    $ sudo yum groupinstall -y "virtualization Host"
    $ sudo yum install -y virt-install virt-manager virt-viewer
    $sudo yum -y install vlgothic-p-fonts


    modprobe kvm
    modprobe kvm-intel
    lsmod | grep kvm
    egrep -c '(vmx|svm)' /proc/cpuinfo