============
Misc.
============

passwd
===========

* ``$ cat /etc/passwd``

::

    　第１フィールド：　ユーザ名（ログイン名）
    　第２フィールド：　（暗号化パスワード）
    　第３フィールド：　ユーザ番号（ＵＩＤ）
    　第４フィールド：　グループ番号（ＧＩＤ）
    　第５フィールド：　コメントフィールド（名前など）
    　第６フィールド：　ホームディレクトリーのパス
    　第７フィールド：　ログイン・シェル



mount
==========

::

   # mount {パーティション} {マウントするパス}
   $ mount /dev/sdb2 /mnt/hdd2


ログインしているユーザを表示
============================

::

   $ w


現在のシェルを確認
====================

::

   $ echo $SHELL

使用可能なシェル一覧
-----------------------

::

   $ cat /etc/shells

source と bash の違い
==========================

* source は　開いた環境でコマンドを実行する
* bash は閉じられた環境でコマンドを実行する




基本
=======

::

   # `` はコマンドの標準出力内容をコマンドに含める（コマンド置き換え）
   $ echo `pwd`

   # $() は``と同等
   $ echo $(pwd)

   # $変数名 ${変数名} は変数に置き換えられる（変数置き換え）

   # "" はワイルドカードを無効にするが、コマンド・変数は置き換えられる
   echo "*" -> *
   echo "$(date)" -> 2018年 4月 9日 月曜日 14時31分05秒 JST

   # '' は文字列をそのまま表示する
   $ echo '$(date)' -> $(date)

   # ヒアドキュメント
   $ cat <<END
   $ ここから
   $ ここまで
   $ END


bashでタブ文字
==================

* ``controll + v`` then press ``Tab``



CentOS
=============


yum
------

List installed repositories
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    $ yum repolist all


List available packages which can be installed
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


::

    $ yum list
    

List installed packages
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    $ yum list installed


Install package with specific repository
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    $ yum install --enablerepo=<repository_name1>,<repository_name2> <package_name>




rpm
--------

List rpm installed packages
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    $ rpm -qa

Uninstall specific package
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    $ rpm -e <package_name>



x window system
---------------------

::

    $ yum groupinstall -y "X Window System"
    $ export DISPLAY=<x_server_address>:0.0

    
