.. maknowledge documentation master file, created by
   sphinx-quickstart on Tue Feb 27 11:10:22 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Maknowledge's documentation.
============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   python/index
   php/index
   db/index
   unix/index
   misc/index

Indices and tables
==================

* :ref:`search`
