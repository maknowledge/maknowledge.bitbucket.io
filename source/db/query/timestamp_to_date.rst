====================================
Timestamp から Date への変換
====================================

CAST演算子で変換する方法（推奨）
====================================

.. code:: sql

    CAST(column1 AS DATE)

※ column1はTimestamp型とします。

TO_CHARでCHAR型に変換した後、TO_DATEでDATE型に変換する方法
============================================================

.. code:: sql

    TO_DATE(TO_CHAR(column1, 'YYYY/MM/DD'),'YYYY/MM/DD')

※ column1はTimestamp型とします。
