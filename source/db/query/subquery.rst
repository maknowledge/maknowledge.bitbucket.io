============
副問い合わせ
============

シンプルな副問い合わせ
========================

-  test1からidが1,2,3のものを抽出し、nameを出力する

.. code:: sql

    select name from table1 where id in(1,2,3)

MAX関数を使った副問い合わせ
====================================

-  test2のheightの最大値を抽出し、test1のnameを出力する

.. code:: sql

    select name
    from table1
    where id = (select max(height) from table2)

副問い合わせとIN句の組み合わせ
====================================

-  test2のheightが100より大きい物(複数ある可能性あり)だけを抽出し、test1のnameを出力する

.. code:: sql

    select name
    from table1
    where id in (select id from table2 where height > 100)

