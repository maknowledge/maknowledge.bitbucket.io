========
内部結合
========

２つのテーブルを結合
=======================

-  table1とtable2をidで結合し、table2のheightが 100
   より大きいものだけをtable1からselectする

.. code:: sql

    select *
        from table1
        inner join table2 on table1.id = table2.id
    where
        table2.height > 100;

.. _つのテーブルを結合-1:

３つのテーブルを結合
==========================

-  table1とtable2とtable3をidで結合しtable2のheightが50より大きく且つtable3のheightが100より大きいものだけをtable1からselectする

.. code:: sql


    select *
       from table1
       inner join table2 on table1.id = table2.id
       inner join table3 on table1.id = table3.id
    where
       table2.height > 50
       and
       table3.height > 100

