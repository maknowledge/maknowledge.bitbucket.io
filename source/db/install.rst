=========
Install
=========


postgresql-9.6
==================

::

    # Repository Package Download
    $ wget https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-7-x86_64/pgdg-centos96-9.6-3.noarch.rpm

    # Repository Package Install
    $ sudo systemctl enable postgresql-9.6

    # Install postgresql-9.6
    $ sudo yum install -y postgresql96-devel postgresql96-server postgresql96-contrib

    # Initialize db
    $ sudo /usr/pgsql-9.6/bin/postgresql96-setup initdb

    # Start service
    $ sudo systemctl start postgresql-9.6

    # Enable Auto start
    $ sudo systemctl enable postgresql-9.6

    # set password to 'postgres' user
    $ sudo passwd postgres

    # change user to 'postgres'
    $ su - postgres

    # login into db
    $ psql -U postgres


* ログイン設定

::

    /var/lib/pgsql/9.6/data/pg_hba.conf の設定を書き換える。

