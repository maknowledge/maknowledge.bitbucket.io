=====
操作
=====


.sql ファイルを実行
==========================

* bashから

::

    $ psql -U <user_name> -d <db_name> -f <path_to_file>

* ログインしてから

::

    $ \i <path_to_file>


list DB
===========

* bashから

::

   $ psql -U <user_name> -d <database> -l -h <host_address>

* ログイン後

::

    $ \l

list tables
================

::

    \d


