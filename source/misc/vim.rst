========
Vim
========


移動
=======

::

   50% ファイルの中央に移動



行数表示
==========

::

   # 表示
   :set number
   
   # 非表示
   :set nonumber


文字コード指定
================

::

   :set encoding=utf-8


ターミナルのエディタにVIMを設定
================================

* 以下の設定を ``.bash_profile`` に追記しておくと良い。

::

   $ export EDITOR=vim
   $ set -o vi


設定ファイル
=================

* ``~/.vimrc``


