======
Foo
======

::

   for entry in *.md;
   do
       pandoc -f markdown -t rst $entry > "${entry%%.md}.rst";
   done


::

   cat /etc/redhat-release



::

   for i in $(find . -type f -name "*.hoge"); do cp ${i} $(echo ${i} | sed 's/\.hoge//'); done


::

   curl http://hoge -X POST -H "Content-Type: application/json" -d '{"hoge": 10}'

::

   pytest --cov-report html --cov=.  test


::

   echo "hoge" 2>& dev/null

::

   get_class(hoge);
   echo get_Class(hoge);
   logger(get_class(hoge));

::

   $ git init --bare --shared path/to/hoge.git
   $ cd path/to/hoge.git
   $ git config http.receivepack true


::

   $ cat docutils.conf
   [parsers]
   smart_quotes: false

   //conf.py と同じディレクトリにファイルを作る

::

    # vi ~/.vimrc
    ---- vi追記 ----
    set encoding=utf-8
    set fileencodings=iso-2022-jp,euc-jp,sjis,utf-8
    set fileformats=unix,dos,mac

::

    {
        "description": "Change left_shift to left_shift. (Post escape if pressed alone)",
        "manipulators": [
            {
                "from": {
                    "key_code": "left_shift",
                    "modifiers": {
                        "optional": [
                            "any"
                        ]
                    }
                },
                "to": [
                    {
                        "key_code": "left_shift"
                    }
                ],
                "to_if_alone": [
                    {
                        "key_code": "escape"
                    }
                ],
                "type": "basic"
            }
        ]
    }

::

   ${parameter#word}
   ${parameter##word}
   Remove matching prefix pattern. The word is expanded to produce a pattern just as in pathname expansion. If the pattern matches the beginning of the value of parameter, then the result of the expansion is the expanded value of parameter with the shortest matching pattern (the # case) or the longest matching pattern (the ## case) deleted. If parameter is @ or *, the pattern removal operation is applied to each positional parameter in turn, and the expansion is the resultant list. If parameter is an array variable subscripted with @ or *, the pattern removal operation is applied to each member of the array in turn, and the expansion is the resultant list.

   ${parameter%word}
   ${parameter%%word}
   Remove matching suffix pattern. The word is expanded to produce a pattern just as in pathname expansion. If the pattern matches a trailing portion of the expanded value of parameter, then the result of the expansion is the expanded value of parameter with the shortest matching pattern (the % case) or the longest matching pattern (the %% case) deleted. If parameter is @ or *, the pattern removal operation is applied to each positional parameter in turn, and the expansion is the resultant list. If parameter is an array variable subscripted with @ or *, the pattern removal operation is applied to each mem- ber of the array in turn, and the expansion is the resultant list.


::

    1.ML110G7をESXiのDVDからブートする
    2.VMインストーラー起動直後にShift+Oを押す（画面下部に記載あり）
    3.出てきたコマンドの後ろにそのまま(スペース)noIOMMUを入力してし起動を進める
    4.インストールし完了したらDVDメディアを取り出して、再起動する
    5.また最初の起動画面でShift Oを押す（画面下部に記載あり)
    6.出てきたコマンドの後ろにそのまま(スペース)noIOMMUを入力してエンター
    7.VMが起動するので、トラブルシューティングオプションからSSHを使用するよう設定する。
    　またIPアドレスを振って外部からSSH接続できるようにしておく。
    8.外部のPCからSSH接続（チャレンジレスポンス）で接続し、そのあとに
    　vi /bootbank/boot.cfg
    　開いたらkerneloptの項目の後ろに(スペース)noIOMMUを入力し保存する。

    利用したイメージ
    VMware-VMvisor-Installer-6.0.0.update02-3620759.x86_64.iso

    以上


::

   # Chrome をリモートデバッグモードで起動する方法
   $ /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome  --remote-debugging-port=9222 --user-data-dir=remote-profile

   # クロスサイトリクエストを許容する設定で起動する方法
   $ /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --args --disable-web-security --user-data-dir

   # 併せる
   $ /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --args --disable-web-security --user-data-dir --remote-debugging-port=9222

::

   # Vagrant
   vagrant destroy
   vagrant init
   vagrant init <base_box_name>
   vagrant halt
   vagrant up
   vagrant provision
   vagrant package --base <vm_name>


   ~/.vagrant.d/boxes

::

   # Java
   ES_JAVA_OPTS=-Xms400m -Xmx400m


::

   $ git checkout $(echo $(git status -s | grep -v '.*.pptx' | grep -v '*\?\? *' | sed -n 's/[ ]*[MD]* \(.*\)/\1/p'))
   
::

   $ i=1; while [ $i -le 20 ]; do j=1; while [ $j -le $i ]; do echo -n '*'; j=$(expr $j + 1); done; echo " ";  i=$(expr $i + 1); done;

::

   # yum
   libzip-devel


::

    # jar ファイル作成
    $ jar cvf <jar_file_name> -C <target_directory> .
    

ToDo memo
===========

::

   # Unix Commands
   w
   eval
   tar
   for
   while
   parted
   ps
   alias
   unalias
   pushd
   popd
   source
   export 
   fstab
   blkid
   echo ${foo##.sh}　とか
   unset
   printenv
   ${foo[*]}
   ${#foo[*]}
   ${#foo}

   
   
   # misc
   Sphinx
   Bash
   git
   sklearn
   pytest
   Ansible
   Linux
   vim
   Docker
   flask
   Swagger
   laravel
   curl
   環境変数など
   SQL
   Makefile
   BNF
   shift + enter(サジェストを無視）

   # laravel
   View::share

   

