===========
Kotlin
===========


クラス名の取得
================

static
---------

::

    val jclass = String::class.java
    val kclass = String::class.java.kotlin


instance
------------

::

    val jclass = s.javaClass
    val kclass = s.javaClass.kotlin 


misc
=========

::

    fun getCounter(): ()->Int {
        var count = 0

        return {
            count++
        }
    }

    fun main(args: Array<String>){
        counter1 = getCounter()
        counter2 = getCOunter()
        counter1()
        counter1()
        counter2()
        counter1()
    }