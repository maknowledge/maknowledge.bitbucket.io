==================
Javascript
==================


DOM
=======

よく使うノードの種類
-----------------------

* Document
* Element
* Text
* Comment
* Attr


外部js読み込み
-------------------

::

    <script type="text/javascript" src="example1.js"></script>


基本
=======

オブジェクト名取得
--------------------

::

   # プロパティを参照する方法
   var foo = FOO();
   var fooName = foo.constructor.name;
   console.log(fooName);

   # Objectのメソッドを使う方法

   var fooName2 = Object.prototype.toString.call(foo);
   console.log(fooName2);


Event Listener
=====================

::

    <html>
        <body>
            <ul id="list">
            <li>item1</li>
            <li>item2</li>
            <li>item3</li>
            <li>item4</li>
            <li>item5</li>
            </ul>
            <button>Add</button>
        </body>
    </html>

::

    const list = document.getElementById('list');
    const btn = document.getElementsByTagName('button').item(0);


    const getTextOnClick = () => {
        const lis = document.getElementsByTagName('li');
        Array.prototype.forEach.call(lis, (v) => {
            v.addEventListener('click', (e) => {
                console.log(e.currentTarget.textContent);
            });
        });
    };

    const addItem = () => {
        btn.addEventListener('click', () => {
            const num = list.childElementCount + 1;
            const li = document.createElement('li');
            li.textContent = `item${num}` ;		
            list.appendChild(li);
            getTextOnClick();
        });
    };

    addItem();
    getTextOnClick();
