=========
Gradle
=========


Intellij IDEA との連携
===================================

Dagger2にソースを認識させる
---------------------------------------

* ``build.gradle`` に以下を追記する場合

::

    apply plugin: 'idea'

    idea {
        module {
            sourceDirs += file('build/generated/source/kapt/main')
            generatedSourceDirs += file('build/generated/source/kapt/main')
        }
    }


* ソースルートを ``build/generated/source/kapt/main/``  に設定する場合。

ディレウトリを右クリックし、``Mark Directory As`` から ``project root`` にする。
