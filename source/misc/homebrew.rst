============
Homebrew
============


Commands
===========


* 現在インストールされているもの

::

   $ brew list


* アプリケーションを検索

::

   $ brew search FOO


brew tap
=============

* 公式以外の formula を追加する。

::

   # GitHubから取り込む
   $ brew tap {user_name}/{repository} 

   # GitHub以外から取り込む
   $ brew tap <URL>

* 取り込んだものを一覧表示する

::

   $ git tap

untap
----------

::

   $ brew upntap {user_name}/{repository}


