========================
Apache HTTP Server
========================

設定ファイル
===============

* ``httpd.conf`` と ``conf.d`` 配下の ``*.conf`` の関係
   * ``/etc/httpd/conf/httpd.conf`` の内部で ``/etc/httpd/conf.d/*.conf`` が読み込まれている。


* ``.htaccess`` の有効化・無効化
   * httpd.conf の内部の `` AllowOverride None`` を ``ALL`` に書き換える。

