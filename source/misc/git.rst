===
Git
===

現在の作業を破棄
==================

::

   $ git checkout .


* 新規作成したファイルがある場合は、それを削除

::

   $ rm -rf `git status --short | grep '??' | awk '{print $2}'`


rebase を取り消す
=====================

::

   $ git rebase --abort


色々取り消す
=====================

:: 

   $ git reflog

取り消したい時点を見つけ、リセットする

::

   $ git reset --hard HEAD@{3}

   

リモートブランチを削除
============================

::

    # git push --delete <remote_name> <branch_name>
    $ git push --delete origin branch_foo

    or

    # git push <remote_name> :<branch_name>
    $ git push origin :branch_foo

ローカルブランチを削除
=============================

::

    # git branch -d <branch_name>
    $ git branch -d branch_foo

    # force
    # git branch -D <branch_name>
    $ git branch -D branch_foo


リモートブランチ一覧
==============================

::

    $ git branch -a


リモートブランチをローカルに
================================

::

    # git checkout -b <local_branch_name> <remote_branch_name>
    $ git checkout -b foo_branch origin/foo_branch


間違えたブランチで作業した場合の対処
===============================================

* 一旦、誤ったブランチでコミットを行う。
* コミットIDをコピーする。
* 変更したかったブランチに移動。
* ``cherry-pick`` を行う。

::

    $ git cherry-pick <commit_id>

* 複数コミットをまとめてコピーしたい場合は、 ``$ git cherry-pick <commit_id_older>..<commit_id_later>``
    

未追跡ファイルの削除
======================

通常
------
::

   # 削除されるファイルを事前確認
   $ git clean -n

   # 削除実行（全て）
   $ git clean -f

   # 削除実行（指定）
   $ git clean -f {path}

オプション
------------
* ``-d`` 追跡対象外のカレントディレクトリ内のディレクトリを削除。
* ``-xf`` カレントディレクトリ内の ``.gitignore`` に含まれるファイルを削除。

空のディレクトリを管理対象にする
=================================

* ディレクトリ内に ``.gitkeep`` ファイルを作成する。

追跡対象から外す
===================

通常
------

::

   $ git rm --cached {path}


便利
-------

::

   $ git rm --cached `git ls-files --full-name -i --exclude-from=.gitignore`


特定のコミットの特定のファイルを取り出す
=============================================

::

    # git show <commit_id>:<file_path>
    $ git show 74hafha:path/to/file.kt

コミットログの修正
==============================

直前
---------
::

    $ git commit --amend

数コミット前
------------------

::

    $ git rebase -i <commit_id>
    
    <commit_id>には修正したいコミットの一つ前のコミットID。
    修正したいコミットの `pick` を `edit` に修正し、保存。
    必要に応じて $ git rebase --continue を行う。

    rebaseが終わったら、コミットIDにブランチを割り当てる。
    $ git branch <new_branch_name> <HEAD>


ブランチの上書き
=====================

::

    $ git checkout <target_branch_name>
    $ git reset --hard <source_branch_name>

gitについて
===============

* working tree: 最新のファイル状態
* index(stage): working tree から add した後の index の状態
* HEAD:  最新のコミットID
* HEAD^: １つ前のコミットID


reset について
==================
基本的な使い方
---------------

::

   git reset COMMIT_ID


COMMIT_ID には　``$ git log`` でリセットしたい id を探すか、HEAD, HEAD^ を用いる。


オプションについて
-------------------

何もオプションを指定しないと ``--mixed`` と同じ扱いになる。

.. list-table::
   :header-rows: 1

   * - オプション名
     - 内容
   * - --hard
     - COMMIT_ID の状態まで commit, working tree, index(stage) をリセットする。
   * - --mixed
     - COMMIT_ID の状態まで commit, index(stage) をリセットするが、working tree はそのまま。
   * - --soft
     - COMMIT_ID の状態まで commit をリセットする。working tree, index(stage) はそのまま。


例
--

* １つ前のコミットに戻し、working tree, index(stage)もリセットする

::

   $ git reset --hard HEAD^

* add を取り消す（全てのファイル）

::

   $ git reset HEAD .

* add を取り消す（特定のファイル）

::

   $ git reset HEAD somefile

* 何も起こらない（はず）

::

   $ git reset --soft HEAD

便利
------
* ブランチを移動したい時に working tree に未コミットのソース等があって移動できない時、とりあえずコミットして行きたいブランチに移動する。元のブランチに戻って作業を継続したいときは ``$ git reset --soft HEAD^`` で元に戻せる。
* 特定のファイルだけをリセットしたい（reset --hard)したいけどresetコマンドではできないので、代わりに git checkout を使う。

::

   $ git checkout HEAD -- hoge.txt


日本語対応
==============

::

   $ git config --local core.quotepath false

* global に適用したい場合は ``--local`` の代わりに ``--global`` を用いる。