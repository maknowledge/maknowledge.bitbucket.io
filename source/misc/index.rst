======
Misc.
======

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   shortcuts
   git
   screen
   docker
   vim
   homebrew
   vagrant
   subversion
   gitlab
   apache_http_server
   javascript
   kotlin
   gradle
   xquartz
   android
   foo
   
