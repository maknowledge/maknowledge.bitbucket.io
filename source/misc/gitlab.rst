============
GitLab
============


キャッシュクリア
===================

::

    $ sudo gitlab-rake cache:clear

再構築
==========

::

    $ sudo gitlab-ctl reconfigure


再起動
========

::

    $ sudo gitlab-ctl restart


Postgres
=============

ログイン方法
---------------

::

    $ gitlab-psql -d gitlabhq_production


DB初期化?
==============

::

    $ sudo gitlab-rake setup