=====================
screen（作成途中)
=====================

インストール(for CentOS)
==============================

::

   $ yum -y install screen


使い方
============

.. list-table::
   :header-rows: 1

   * - 項目
     - コマンド
   * - スクリーン作成
     - screen
   * - スクリーン作成（名前付き）
     - screen -S PID
   * - スクリーン一覧
     - screen -ls
   * - アタッチ
     - screen -r PID
   * - アタッチ（強制）
     - screen -rx PID
   * - デタッチ
     - Control + a -> d
   * - スクリーンを全削除
     - screen -r -X quit
   * - 特定のスクリーンを削除
     - screen kill PID
   * - 異常なスクリーンを削除
     - screen -wipe
   * - 全ウィンドウ終了
     - Control + a -> \\
   * - ウィンドウ作成
     - Control + a -> c
   * - ウィンドウ切り替え（次へ）
     - Control + a -> n
   * - ウィンドウ切り替え（前へ）
     - Control + a -> p
   * - ウィンドウ一覧
     - Control + a -> "
   * - 現在のウィンドウに名前を付ける
     - Control + a -> A
   * - 現在のウィンドウを閉じる
     - Control + a -> k
   * - コピー
     - Control + a -> [ 

       コピーしたい箇所に移動して space 

       コピー範囲を選択して space 
   * - ペースト
     - Control + a -> ]

