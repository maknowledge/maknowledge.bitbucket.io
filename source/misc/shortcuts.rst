==========================
Shortcuts(for Mac)
==========================

Mac OS X
============

.. list-table:: shortcuts for Mac OS X
   :header-rows: 1

   * - 機能
     - ショートカットキー
   * - デスクトップ表示
     - Command + F3
   * - スクリーンショット（画面全体）
     - Command + Shift + 3
   * - スクリーンショット（範囲選択）
     - Command + Shift + 4
   * - スクリーンショット（画面選択）
     - Command + Shift + 4 + Space 
   * - 全画面/全画面解除
     - Command + Control + F
   * - 隠しファイル（ディレクトリ）表示
     - Command + Shift + .
     

Chrome
============

.. list-table:: shortcuts for Chrome
   :header-rows: 1

   * - 機能
     - ショートカットキー
   * - 進む
     - Command + [ 
   * - 戻る
     - Command + ]
   * - 更新
     - Command + R
   * - 1画面分スクロール（下へ）
     - Space
   * - 1画面分スクロール（上へ）
     - Shift + Space
   * - 新しいタブ
     - Command + T
   * - タブを閉じる
     - Command + W
   * - タブ選択（次へ）
     - Control + Tab
   * - タブ選択（前へ）
     - Control + Shift + Tab
   * - 閉じたタブを開き直す
     - Command + Shift + T
   * - 新しいウィンドウ
     - Command + N
   * - 新しいウィンドウ（シークレット）
     - Command + Shift + N
   * - ディベロッパーツール
     - Command + Option + I
   * - インスペクタモード
     - Command + Shift + C
   * - Chromeを終了する
     - Command + W


PyCharm
============

.. list-table:: shortcuts for PyCharm
   :header-rows: 1

   * - 機能
     - ショートカット
   * - Quick Definition
     - Option + Space もしくは Command + Y
   * - Quick Documentation
     - Control + J 

iTerm2
============

.. list-table:: shortcuts for iTerm2
   :header-rows: 1

   * - 機能
     - ショートカット
   * - 画面分割（横）
     - Command + D
   * - 画面分割（縦）
     - Command + Shift + D 
   * - 新しい画面
     - Command + N 
   * - 全画面/全画面解除
     - Command + Enter
   * - 新規タブ
     - Command + T
   * - タブ間移動
     - Command + カーソルキー
   * - 指定ウィンドウを全画面/全画面解除
     - Command + Shift + Enter


Intellij IDEA
====================

.. list-table:: shortcuts for Intellij IDEA
   :header-rows: 1

   * - 機能
     - ショートカットキー
   * - ファイル検索
     - Command + Shift + O
   * - クラス検索
     - Command + O
   * - シンボル検索
     - Command + Option + O
   * - 全文検索
     - Command + Shift + F
   * - 定義ジャンプ
     - Command + B


VSCode
==================

.. list-table:: shortcuts for VSCode
   :header-rows: 1

   * - 機能
     - ショートカットキー
   * - フォールド
     - Command + K Command + 0
   * - フォールド解除
     - Command + K Command + J
   * - 整形
     - Command + Option + L
      