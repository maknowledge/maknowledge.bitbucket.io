==========
Docker
==========

Commands
============

起動中のコンテナを確認
-------------------------

::

   $ docker ps


起動中のコンテナ内に入る
---------------------------

::

   $ docker ecec -it {container name} bash


イメージ一覧を表示
---------------------

::

   $ docker images


イメージ削除
----------------

::

   $ docker rmi {image id}


コンテナ一覧
----------------

::

   $ docker container ls




docker-compose
====================
IP固定（調整中）
---------------------

::

   services:
     hoge:
       image: hogehoge
       networks:
         hoge:
           ipv4_address: 127.0.0.5
   networks:
     hoge:
       ipam:
         config:
           - subnet: 127.0.0.1/8


misc
--------

::

   $ docker network ls
   $ docker network inspect Foo
   $ docker network rm Foo
   $ docker stop 


image の実態がある場所（Mac)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   ~/Library/Containers/com.docker.docker/Data/com.docker.driver.amd64-linux/Docker.qcow2

Commands
-------------

down
^^^^^^

コンテナの停止と、コンテナとネットワークを削除。

``--rmi all`` オプションで、イメージも削除。

::

   $ docker-compose down


kill
^^^^^^^

コンテナの停止

::

   $ docker-compose kill

up
^^^^

コンテナの起動

::

   $ docker-compose up
   

