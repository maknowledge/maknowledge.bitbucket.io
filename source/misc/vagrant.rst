====================
Vagrant
====================


Vagrantfile
==============

* 起動時にシェルスクリプトを実行する方法（/etc/profile.d/　配下のシェルスクリプトが実行される）

::

    config.vm.provision "export env",
        type: "shell",
        run: "always",
        privileged: true,
        inline: <<-SHELL
            echo "# vagrant script for every boot" > /etc/profile.d/vagrant.sh
            echo export XDEBUG_CONFIG=remote_host=192.168.11.25 >> /etc/profile.d/vagrant.sh
            chmod +x /etc/profile.d/vagrant.sh
        SHELL
