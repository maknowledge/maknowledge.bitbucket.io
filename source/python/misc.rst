========
Misc.
========


モジュールファイルをダウンロードする方法
================================================

::

   $ pip download <module_name> -d "./"

* ``-d, --dest <dir>``
   * Download packages into <dir>.


ネットワークがない環境でモジュールをインストールする方法
=============================================================

::

   $ pip install <module_file_name> -f ./ --no-index

* ``-i, --index-url <url>``
   * Base URL of Python Package Index (default https://pypi.python.org/simple). This should point to a repository compliant with PEP 503 (the simple repository API) or a local directory laid out in the same format.

* ``-f, --find-links <url>``
   * If a url or path to an html file, then parse for links to archives. If a local path or file:// url that's a directory, then look for archives in the directory listing.





