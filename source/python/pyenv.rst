========
Pyenv
========

インストール
====================

下記のコマンドを実行

.. code:: bash

    $ brew install pyenv
    $ brew install pyenv-virtualenv 

併せてPathを通す為に、~/.bash_profileに下記内容を追記する

.. code:: bash

    export PYENV_ROOT="$HOME/.pyenv"
    export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"

追記し終わったら　source ~/.bash_profile を実行し、環境変数を適用させる

仮装環境作成
====================

まずpyenvでpythonをインストールする 下記コマンドはpython
3.6.3をインストールする例

.. code:: bash

    $ pyenv install 3.6.3

次に、pyenv-virtualenvで環境を区切る 下記コマンドでは、python 3.6.3 の
piyoという名前の環境を作る

.. code:: bash

    $ pyenv virtualenv 3.6.3 piyo

作成した仮装環境一覧は pyenv versions で確認できる

.. code:: bash

    $ pyenv versions
    * system(set by /Users/sample/.pyenv/version)
      3.6.3
      3.6.3/envs/piyo
      piyo 

仮装環境適用
====================

現在のディレクトリに仮想環境を適用する場合
---------------------------------------------

.. code:: bash

    $ pyenv local piyo

環境が適用されたかどうかは pyenv version で確認できる

.. code:: bash

    $ pyenv version
    piyo (set by /Users/sample/.pyenv/version)

全体の環境に仮装環境を適用する場合
------------------------------------

.. code:: bash

    $ pyenv global piyo

一時的に仮装環境を適用する場合
------------------------------------

.. code:: bash

    $ pyenv activate piyo

一時的に適用した仮装環境を外す場合
--------------------------------------

.. code:: bash

    $ pyenv deactivate

仮装環境をアンインストールする場合
------------------------------------

.. code:: bash

    $ pyenv uninstall piyo

余談
----

local
で環境を適用した場合、そのディレクトリに.python-version　というファイルができる。
その中に仮装環境名が記載されている。
