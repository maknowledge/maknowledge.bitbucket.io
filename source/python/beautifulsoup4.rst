==================
BeautifulSoup4
==================

コード
============

シンプルに http://google.com からタイトルを取得し表示します。

.. code:: python

    import requests
    from bs4 import BeautifulSoup

    URL = 'http://google.com'
    headers = {"User-Agent": "hoge"}

    resp = requests.get(URL, timeout=1, headers=headers)
    r_text = resp.text

    soup = BeautifulSoup(r_text, 'html.parser')
    soup_titles = soup.find_all('title')

    for t in soup_titles:
        print(t.get_text())

実行結果

.. code:: bash

    Google

説明
============

1
-

.. code:: python3

    import requests
    from bs4 import BeautifulSoup

今回はrequestsを使いHttp経由でデータを取得し、BeautifulSoupでタグをパースするので上記２つをインポートします。入っていない場合は下記の様にpip
installします。

.. code:: bash

    $ pip install requests
    $ pip install beautifulsoup4

.. _section-1:

2
-

.. code:: python3

    URL = 'http://google.com'
    headers = {"User-Agent": "hoge"}
    resp = requests.get(URL, timeout=1, headers=headers)
    r_text = resp.text

URLに取得元のURLを指定。 headersにリクエストヘッダを指定。
requests.getメソッドでデータを取得。

.. _section-2:

3
-

.. code:: python3

    soup = BeautifulSoup(r_text, 'html.parser')

取得したテキストを元にBeautifulsoupオブジェクトを取得

.. _section-3:

4
-

.. code:: python3

    soup_titles = soup.find_all('title')

titleタグのセットを取得

.. _section-4:

5
-

.. code:: python3

    for t in soup_titles:
        print(t.get_text())

セットからタグ要素を取得し、テキスト部分を表示
